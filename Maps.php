<?php
namespace app\library;
/*
Class Name: maps
Version: 1.1
Description: Gestione delle mappe, recupero coordinate e calcolo distanze
Author: Michael Di Pietro
Author URI: https://www.facebook.com/dpmika
*/

class Maps {
  // Crea la chiave qui https://developers.google.com/maps/documentation/geocoding/get-api-key
  const apiKey = 'La tua api key google';


  public static function getCoordinate($cap, $address, $city){
    $cap      = str_ireplace(" ", "+", $cap);
    $address  = str_ireplace(" ", "+", $address);
    $city     = str_ireplace(" ", "+", $city);
    $address = $cap.",".$address.",".$city;
    $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".$address."&key=".self::apiKey;
    $response = file_get_contents($url);
    $json = json_decode($response,TRUE);
    $coordinate = array(
      'lat' => $json['results'][0]['geometry']['location']['lat'],
      'lon' => $json['results'][0]['geometry']['location']['lng']
    );
    return $coordinate;
  }

  public static function distanza($punto1, $punto2) {
    $lat1 = $punto1['lat'];
    $lon1 = $punto1['lon'];
    $lat2 =$punto2['lat'];
    $lon2 = $punto2['lon'];
    $distance = (3958*3.1415926*sqrt(($lat2-$lat1)*($lat2-$lat1) + cos($lat2/57.29578)*cos($lat1/57.29578)*($lon2-$lon1)*($lon2-$lon1))/180);
    $miglia = 1.6093;
    $distanceKm = round($distance*$miglia, 2);
    return $distanceKm;

  }

}
?>
